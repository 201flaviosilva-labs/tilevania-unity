using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement :MonoBehaviour {
    // Editor
    [SerializeField] float playerSpeed = 10;
    [SerializeField] float jumpSpeed = 10;
    [SerializeField] float climSpeed = 10;

    // Components
    Rigidbody2D rigidbody2D;
    CapsuleCollider2D capsuleCollider2D;
    Animator animator;

    // Private
    Vector2 _moveInput;
    float _gravityScale = 2;

    private void Awake() {
        rigidbody2D = GetComponent<Rigidbody2D>();
        capsuleCollider2D = GetComponent<CapsuleCollider2D>();
        animator = GetComponent<Animator>();
        _gravityScale = rigidbody2D.gravityScale;
    }

    void Start() {

    }

    void OnMove(InputValue value) {
        _moveInput = value.Get<Vector2>();
    }

    void OnJump(InputValue value) {
        // If space is pressed and the player is touching in the ground layer
        if (value.isPressed && capsuleCollider2D.IsTouchingLayers(LayerMask.GetMask("Ground"))) {
            rigidbody2D.velocity += new Vector2(0, jumpSpeed);
        }
    }

    void Update() {
        // Move
        Vector2 playerVelocity = new Vector2(_moveInput.x * playerSpeed, rigidbody2D.velocity.y);
        rigidbody2D.velocity = playerVelocity;

        // Running Animation
        bool isMovingHorizontally = Mathf.Abs(rigidbody2D.velocity.x) > Mathf.Epsilon;
        animator.SetBool("isRunning", isMovingHorizontally);

        flipXSprite();

        climbLadder();
    }

    void flipXSprite() {
        bool isMovingHorizontally = Mathf.Abs(rigidbody2D.velocity.x) > Mathf.Epsilon;
        if (isMovingHorizontally) {
            transform.localScale = new Vector2(Mathf.Sign(rigidbody2D.velocity.x), 1);
        }
    }

    void climbLadder() {
        if (capsuleCollider2D.IsTouchingLayers(LayerMask.GetMask("Climbing"))) {
            rigidbody2D.gravityScale = 0;
            Vector2 climbVelocity = new Vector2(rigidbody2D.velocity.x, _moveInput.y * climSpeed);
            rigidbody2D.velocity = climbVelocity;
            animator.SetBool("isClimbing", Mathf.Abs(rigidbody2D.velocity.y) > Mathf.Epsilon);
        } else {
            animator.SetBool("isClimbing", false);
            rigidbody2D.gravityScale = _gravityScale;
        }
    }
}
